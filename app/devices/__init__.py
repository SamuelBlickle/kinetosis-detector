from flask import g
from app.models import Session


deviceClasses = {}


def register_device_class(cls):
    deviceClasses[cls.__name__] = cls
    return cls


from app.devices.dummy_device import DummyDevice
