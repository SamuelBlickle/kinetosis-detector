from datetime import datetime
from app import db
from sqlalchemy.ext.declarative import declared_attr


class BaseDevice(db.Model):
    __abstract__ = True

    id = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)

    @declared_attr
    def id_session(cls):
        return db.Column(db.Integer, db.ForeignKey('session.id'))

    def to_dict(self):
        data = {
            'id': self.id,
            'timestamp': self.timestamp,
            'id_session': self.id_session
        }
        return data
